<?php

use Illuminate\Support\Facades\Route;


Route::name('front.')->group(function () {
   
    Route::get('/', 'AccountController@create')->name('accounts.create');
    
});

Route::middleware('auth')->group(function() {

    Route::name('admin.dashboard.')->group(function () {

        Route::get('/admin', 'AccountController@index')->name('accounts.list');
        Route::post('/admin/accounts', 'AccountController@store')->name('accounts.store');
        Route::get('/admin/accounts/{account}/edycja', 'AccountController@edit')->name('accounts.edit');
        Route::patch('/admin/accounts/{account}/edycja', 'AccountController@update')->name('accounts.update');
        Route::delete('/admin/accounts/{account}', 'AccountController@destroy')->name('accounts.destroy');

    });

});

Route::namespace('Auth')->group(function () {
    Route::get('login', 'FormController@showLoginForm')->name('login');
    Route::get('password/reset', 'FormController@showLinkRequestForm')->name('password.request');
    Route::get('password/reset/{token}', 'FormController@showResetForm')->name('password.reset');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');

    Route::middleware('auth')->group(function() {
        Route::get('password/change', 'ChangePasswordController@edit')->name('password.change');
        Route::put('password/change', 'ChangePasswordController@update')->name('password.change.update');
    });
});