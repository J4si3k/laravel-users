#!/bin/sh

# Wait until MySQL is ready
while ! docker-compose run --rm mysql mysqladmin ping -h"mysql" -P"3306" --silent; do
    echo "Waiting for MySQL to be up..."
    sleep 1
done