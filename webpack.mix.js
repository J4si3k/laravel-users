const mix = require('laravel-mix');

mix
.webpackConfig({
    module: {
        rules: [
            {
              test: /\.m?js$/,
              exclude: /(node_modules|bower_components)/,
              use: {
                loader: 'babel-loader',
                options: {
                  presets: ['@babel/preset-env'],
                  plugins: [ "array-includes"]
                }
              }
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    {
                        loader: 'postcss-loader',
                        options: {
                          config: {
                              path: './'
                          }
                        }
                    }
                ],
              },
        ]
    }
})
.js('resources/js/front/app.js', 'public/js')
.js('resources/js/admin/admin.js', 'public/js')
.sass('resources/sass/app.scss', 'public/css')
.sass('resources/sass/admin.scss', 'public/css')
.version()
.browserSync('http://localhost:8000');
