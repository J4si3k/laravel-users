<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Arr;

class Account extends Model
{
    protected $fillable = [
        'name', 'email', 'surname', 'description', 'position_id'
    ];

    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    public function skills()
    {
        return $this->belongsToMany(Skill::class)->withPivot('skill_value');
    }

    function toArrayWithSkills() 
    {
        return array_merge(
            collect($this->only('name', 'surname', 'description'))->toArray(),
            [
                'skills' => $this->skills->map(function ($skill) {
                    $skillWithPivot = array_merge($skill->toArray(), ['skill_value' => Arr::get($skill, 'pivot.skill_value')]) ;
                    return collect($skillWithPivot)
                        ->only(['name', 'skill_value'])
                        ->all();
                })->toArray()
            ]
        );
    }
    
}
