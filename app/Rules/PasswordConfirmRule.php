<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Auth;
use Hash;

class PasswordConfirmRule implements Rule
{
    public function passes($attribute, $value): bool
    {
        return Hash::check($value, Auth::user()->getAuthPassword());
    }

    public function message()
    {
        return 'Your current password is incorrect.';
    }
}
