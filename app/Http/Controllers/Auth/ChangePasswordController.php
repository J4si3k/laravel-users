<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UpdatePasswordRequest;
use App\Jobs\UpdatePassword;
use Auth;

class ChangePasswordController extends Controller
{
    public function edit()
    {
        return view('admin.auth.passwords.change');
    }

    public function update(UpdatePasswordRequest $request)
    {
        $this->dispatchNow(new UpdatePassword(Auth::user(), $request->newPassword()));
        return redirect()->route('admin.dashboard.accounts.list')->withSuccess('zaktualizowano hasło');
    }
}
