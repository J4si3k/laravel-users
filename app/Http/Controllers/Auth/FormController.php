<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UpdatePasswordRequest;
use Illuminate\Support\Facades\Hash;
use App\User;
use Auth;

class FormController extends Controller
{
    
    public function showLoginForm() 
    {
        return view('admin.auth.login');
    }

    public function showLinkRequestForm() 
    {
        return view('admin.auth.passwords.email');
    }

    public function showResetForm($token) 
    {
        $email = request()->get('email');
        return view('admin.auth.passwords.reset', compact('token', 'email'));
    }

}
