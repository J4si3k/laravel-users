<?php

namespace App\Http\Controllers;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Database\QueryException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Jobs\AccountUpdate;
use App\Jobs\AccountSkillsStore;
use App\Http\Requests\AccountRequest;
use App\Mail\AccountCreated;
use App\Account;
use App\Skill;
use App\Position;
use Exception;
use Validator;
use DB;
use Mail;
use Log;
use Arr;


class AccountController extends Controller
{
   
    public function index()
    {
        $accounts = Account::with('position:id,name')
                            ->with('skills:name')
                            ->get();

        return view('admin.pages.accounts.list', [
            'accounts' => $accounts
        ]);
    }


    public function create()
    {
        $positions = Position::select('id','name')
                             ->with('skills:name,type')
                             ->get();

        return view('front.pages.accounts.register', compact('positions'));
    }


    public function store(AccountRequest $request)
    {
 
        try {
            $validated = $request->validated();
            
            DB::beginTransaction();
                $account = Account::create(Arr::except($validated, ['skills']));
                $this->dispatchNow(new AccountSkillsStore($account, $validated['skills'])); 
                $account->save();
                dispatch(function () use ($account) {
                    Mail::to($account['email'])->send(new AccountCreated($account->toArrayWithSkills()));
                })->afterResponse();
            DB::commit();
            
            return response()->json(['messages' => ['Pomyślnie zarejestrowano konto']]);
           
        } catch (ValidationException | QueryException | Exception $e) {
            logger($e);
            DB::rollback();
            if ($e instanceof ValidationException) $message = $e->validator->errors()->all();
            if ($e instanceof QueryException) $message[] = ($e->errorInfo[1] === 1062) ? 'Account duplicate error' : 'QueryException. Something went wrong';
            if ($e instanceof Exception) $message[] = 'Something went wrong';
            throw new HttpResponseException(response()->json(['errors' => $message], 422)); 
        }

    }

 
    public function edit(Account $account)
    {
        $accounts = Account::with('skills:id,name')
                        ->with('position:id,name')
                        ->findOrFail($account->id);

        return view('admin.pages.accounts.form')->with([
            'account' => $account,
            'positions' => Position::pluck('name', 'id'),
            'skills' => Skill::with('accounts')->wherePosition($account->position_id)->pluck('name', 'id')
        ]);
    }


    public function update(AccountRequest $request, Account $account)
    {
        $validated = $request->validated();

        $this->dispatchNow(new AccountUpdate($account, $validated)); 

        return redirect()->route('admin.dashboard.accounts.list')->withSuccess('Zaktualizowano dane użytkownika');
    }


    public function destroy(Account $account)
    {
        $account->delete();

        return redirect()->route('admin.dashboard.accounts.list')->withSuccess('Usunięto uzytkownika');
    }
}
