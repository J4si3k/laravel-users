<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class AccountRequest extends FormRequest
{

    protected $method;

    public function authorize()
    {
        $this->method = $this->method();
        return true;
    }

    public function rules()
    {
        $rules = [
            'name' => 'required',
            'surname' => 'required',
            'description' => 'nullable|string',
            'skills' => 'required|array',
            'skills.*.name' => 'string|exists:skills,name',
            'skills.*.skill_value' => 'nullable|string_or_boolean'
        ];

        if ($this->method === 'POST') {

            $rules['email'] = 'email';
            $rules['position_id'] = 'required|integer|exists:positions,id';
        } 

        return $rules;
    }

    public function messages()
    {

        $messages = [
            'required' => 'Pole :attribute jest wymagane',
            'integer' => 'Pole :attribute nie jest liczbą',
        ];

        if ($this->method === 'POST') {
            $messages['email'] = 'Pole :attribute posiada nieprawidłowy format';
        }

        return $messages;
    }

    protected function failedValidation(Validator $validator) { 
            throw new HttpResponseException(response()->json(['errors' => $validator->errors()->all()], 422)); 
    }
}
