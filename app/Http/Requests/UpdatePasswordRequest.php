<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\PasswordConfirmRule;

class UpdatePasswordRequest extends FormRequest
{
    public function rules()
    {
        return [
            'current_password' => ['sometimes', 'required', new PasswordConfirmRule],
            'password' => 'required|confirmed|min:8',
        ];
    }

    public function newPassword(): string
    {
        return $this->get('password');
    }
}
