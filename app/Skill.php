<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = [
        'name',
        'type'
    ];

    public function positions()
    {
        return $this->belongsToMany(Position::class);
    }
    
    public function accounts()
    {
        return $this->belongsToMany(Account::class)->withPivot('skill_value');
    }

    public function scopeWherePosition($query, $positionId)
    {
        $query->whereHas('positions', function ($query) use ($positionId) {
            $query->where('positions.id', $positionId);
        });
    }
}
