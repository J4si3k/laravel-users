<?php

namespace App\Jobs;

use DB;
use Arr;

class AccountUpdate
{

    protected $account;
    protected $validated;

    public function __construct($account, array $validated)
    {
        $this->account = $account;
        $this->validated = $validated;
    }


    public function handle()
    {
        $vaccount = collect($this->validated)->only('name', 'surname', 'description')->toArray();
        $vSkills = $this->getSkills();

        DB::transaction(function () use ($vaccount, $vSkills) {
            
            $this->account->update($vaccount);

            $relatedIds = $this->account->skills()->allRelatedIds();
            foreach ($relatedIds as $key => $id) {
                $this->account->skills()->updateExistingPivot($id, $vSkills[$key]);
            }
            $this->account->save();
        });
    }


    function getSkills() 
    {
        $skills = collect($this->validated)->only('skills')->first() ?? [];
        return array_map(function($group) {
            return Arr::except($group, ['type']);
        }, $skills);
    }
    
}
