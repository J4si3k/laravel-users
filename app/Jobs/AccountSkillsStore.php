<?php

namespace App\Jobs;
use App\Account;

class AccountSkillsStore
{
    protected $account;
    protected $skills;

    public function __construct(Account $account, array $skills)
    {
        $this->account = $account;
        $this->skills = $skills;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $relatedIds = $this->account->skills()->allRelatedIds();
        foreach ($this->skills as $key => $skill) {

            $this->account->skills()->attach([
                $this->account->id => [
                    'skill_value'=> $skill['skill_value'], 
                    'skill_id'=> $skill['pivot']['skill_id']
                ]
            ]);
        }
    }
}
