<?php

namespace App\Traits;

use File;

trait FactoryTrait
{
    public function getSampleJson($filename) 
    {
        $json = File::get(database_path("data/{$filename}.json"));
        return json_decode($json);
    }

    public function countSampleJson($filename) 
    {
        $data = $this->getSampleJson($filename);
        return count($data);
    }

}