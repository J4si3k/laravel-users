<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;

if (! function_exists('routeContains')) {

    function routeContains($name) 
    {
        if (is_array($name)) {
            foreach($name as $routeName){
                if (Str::contains(Route::current()->getName(), $routeName)) {
                    return true;
                }
            }
            return false;
        } else {
            return Str::contains(Route::current()->getName(), $name);
        }
    }
}

if (! function_exists('humanize_date')) {
    /**
     * Return a formatted Carbon date.
     */
    function humanize_date($date, string $format = 'd F Y, H:i'): string
    {
        return (!is_null($date)) ? $date->format($format) : '';
    }
}

function colorPlus200($color) 
{
    $parts = explode('-', $color);
    $colorValue = last($parts);
    $newColor = intVal($colorValue+200);
    return "$parts[0]-$parts[1]-$newColor";
}