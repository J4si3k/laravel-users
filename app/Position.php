<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $fillable = [
        'name',
    ];

    function accounts() 
    {
        return $this->belongsToMany(Account::class);
    }

    function skills()
    {
        return $this->belongsToMany(Skill::class);
    }
}
