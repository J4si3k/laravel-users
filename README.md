# Setup project with docker compose
1. run command `composer create-project`
2. set mail settings in .env (optional RABBITMQ_HOST, other settings done)
3. run command `composer run setup`
4. login to http://localhost:8000/admin with:
```
login: admin@admin.pl, 
password: password 
```

# Optional
run with RabbitMQ from rabbit branch

