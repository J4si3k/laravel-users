<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('surname');
            $table->mediumText('description')->nullable();
            $table->string('email')->unique();
            $table->timestamps();
        });

        // convention singular name & alphabetic order
        Schema::create('position_skill', function (Blueprint $table) {
            $table->unsignedBigInteger('position_id')->index()->nullable();
            $table->unsignedBigInteger('skill_id');
            $table->foreign('position_id')->references('id')->on('positions')->onDelete('cascade');
            $table->foreign('skill_id')->references('id')->on('skills')->onDelete('cascade');
        });

        Schema::create('account_skill', function (Blueprint $table) {
            $table->unsignedBigInteger('account_id');
            $table->unsignedBigInteger('skill_id');
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
            $table->foreign('skill_id')->references('id')->on('skills')->onDelete('cascade');
            $table->string('skill_value')->nullable();
        });

        Schema::table('accounts', function($table) {
            $table->unsignedBigInteger('position_id')->index()->nullable();
            $table->foreign('position_id')->references('id')->on('positions')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_skill'); 
        Schema::dropIfExists('skill_account'); 
        Schema::dropIfExists('accounts');
    }
}
