<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Skill;
use Faker\Generator as Faker;
use App\Traits\FactoryTrait;

$factory->define(Skill::class, (new class {
    use FactoryTrait;
    
    public function generatorFunction() {
        return function (Faker $faker) {
            
            static $number = 0;
            $i = $number++;

            $data = $this->getSampleJson('skills');
            foreach ($data as $obj) {
                return [
                    "name" => $data[$i]->name,
                    "type" => $data[$i]->type,
                ];
            }

        };
    }
})->generatorFunction());
