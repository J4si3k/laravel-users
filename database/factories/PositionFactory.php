<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Position;
use Faker\Generator as Faker;
use App\Traits\FactoryTrait;

$factory->define(Position::class, (new class {
    use FactoryTrait;
    
    public function generatorFunction() {
        return function (Faker $faker) {
            
            static $number = 0;
            $i = $number++;

            $data = $this->getSampleJson('positions');
            foreach ($data as $obj) {
                return [
                    "name" => $data[$i]->name,
                ];
            }

        };
    }
})->generatorFunction());