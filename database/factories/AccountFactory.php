<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Account::class, function (Faker $faker) {

    $position_id = $faker->numberBetween(1, App\Position::count());

    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'description' => $faker->randomElement([null, null, $faker->text]),
        'email' => $faker->unique()->safeEmail,
        'position_id' => $faker->randomElement([$position_id]),
    ];
});
