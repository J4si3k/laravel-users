<?php

use Illuminate\Database\Seeder;
use App\Traits\FactoryTrait;

class PositionSeeder extends Seeder
{
    use FactoryTrait;


    public function run()
    {
        $iterations = $this->countSampleJson('positions');
        factory(App\Position::class, $iterations)->create(); 
    }
}
