<?php

use Illuminate\Database\Seeder;
use App\Traits\FactoryTrait;

class SkillSeeder extends Seeder
{
    use FactoryTrait;
    
    public function run()
    {
        $iterations = $this->countSampleJson('skills');
        $collection = collect(array_values($this->getSampleJson('skills')));
        factory(App\Skill::class, $iterations)->make()->each(function ($skill, $key) use ($collection) {

            $isInserted = App\Skill::where('name', $skill->name)->first();
            $position = App\Position::where('name', $collection[$key]->position)->first();

            
            if (empty($isInserted)) {
                $skill->fill([
                    'name' => $skill->name,
                    'type' => $skill->type,
                ])->save();
                $skill->positions()->attach($position->id);
                if ($key == 8) $skill->positions()->attach([ 3 => ['skill_id'=> 2]]);
            }

            
        });
    }
}
