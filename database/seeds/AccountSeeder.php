<?php

use Illuminate\Database\Seeder;
use App\Traits\FactoryTrait;
use Faker\Factory as Faker;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Helper\ProgressBar;

class AccountSeeder extends Seeder
{
    use FactoryTrait;

    private $faker;

    public function run()
    {
        $this->faker = Faker::create();
        $output = new ConsoleOutput();
        $this->progress = new ProgressBar($output, 4);
        $this->progress->start();

        

        factory(App\Account::class, 3)->create()->each(function ($account)
        {
            $collection = collect(array_values($this->getSampleJson('skills')));
            $filtered = $collection->where('position', $account->position->name)->all();
            
            foreach ($filtered as $key => $value) {

                $skill = App\Skill::where('name', $value->name)->first();
                $account->skills()->attach([
                    $account->id => [
                        'skill_value'=> $this->faker->randomElement($value->examples), 
                        'skill_id'=> $skill->id
                    ]
                ]);
            }

            $this->progress->advance();
        });

        $this->progress->finish();
    }
}
