module.exports = {
    purge: false,
    theme: {
      inset: {
        '0': 0,
       auto: 'auto',
       '1/2': '50%',
      },
      extend: {
        boxShadow: {
          red: '0 0 0 3px rgba(222, 54, 24, .5)'
        }
      }
    },
    variants: {},
    plugins: [
      require('@tailwindcss/custom-forms')
    ]
  }
  