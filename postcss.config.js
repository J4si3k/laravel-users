module.exports = (env, args) => ({
    plugins: [
        require('postcss-import'),
        require('tailwindcss'),
        require('postcss-flexbugs-fixes'),
        require('postcss-nested'),
        require('autoprefixer'),
        env.file.basename.indexOf('app') !== -1 && env.webpack.mode == 'production' && require('@fullhuman/postcss-purgecss')({
            content: [
                './resources/views/front/**/*.php',
                './resources/js/front/**/*.vue',
            ],
            defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || [],
        }),
        env.file.basename.indexOf('admin') !== -1 && env.webpack.mode == 'production' && require('@fullhuman/postcss-purgecss')({
            content: [
                './resources/views/admin/**/*.php',
                './resources/js/admin/**/*.vue',
            ],
            defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || [],
        }),
        env.webpack.mode == 'production' && require('cssnano')({
            preset: 'default',
        }),
    ]
})