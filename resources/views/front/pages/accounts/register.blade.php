@extends('front.parts.wrapper')
@section('content')

<div class="flex h-screen">
    <div class="m-auto w-full max-w-2xl">

        <div class="text-center bg-white text-gray-700 pl-4 font-bold uppercase px-2 py-3">
            Zarejestruj
        </div>

        <register-form 
        :positions="{{ isset($positions) ? json_encode($positions) : json_encode([]) }}"
        ></register-form>

        <p class="text-center text-gray-500 text-xs">
            © {{ date('Y') }} @lang('All rights reserved.')
        </p>    
    </div>
</div>
@endsection
