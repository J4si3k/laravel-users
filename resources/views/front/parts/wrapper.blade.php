@include('front.parts.header')
<div id="frontapp">

    @yield('content')

</div>

<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
