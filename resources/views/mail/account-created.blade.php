@component('mail::message')
# Witaj <?= $account['name'] ?>

Dziękujemy za rejestrację.  <br><br>
Twoje dane: <br>

@php
$counter = 0;
array_walk_recursive($account, function ($field, $key) use (&$counter)  {
   echo ($key == 'name' && $counter > 1) ? '<br><b>' : '';
   echo  \Blade::compileString("@boolToText($field)");
   echo ($key == 'name' && $counter > 1) ? '</b>' : '';
   echo "<br>";
   $counter++;
}, $counter);
@endphp



{{ config('app.name') }}
@endcomponent
