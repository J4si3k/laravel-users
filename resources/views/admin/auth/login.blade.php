@extends('admin.auth.wrapper')
@section('content')

        <div class="bg-white text-gray-700 pl-4 font-bold uppercase bg-gray-lighter px-2 py-3 border-solid border-gray-light border-b">
            Zaloguj
        </div>
        <form method="POST" action="{{ route('login') }}" class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
            @csrf
            
            <div class="mb-4">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
                    E-Mail Address
                </label>
                <input id="email" type="email" 
                class="@error('email') border-red-500 @enderror shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                 name="email" autofocus value="{{ old('email') ?? ''}}">
                @error('email')
                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                @enderror
            </div>
            <div class="mb-6">
                <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
                    Password
                </label>
                <input
                    class="@error('password') border-red-500 @enderror shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                    id="password" name="password" type="password" value="">
                @error('password')
                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                @enderror
            </div>
            <div class="flex items-center justify-between">
                <button
                    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    type="submit">
                    Zaloguj
                </button>
                @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}" class="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800">
                        Zapomniałeś hasła?
                    </a>
                @endif
            </div>
        </form>
        <p class="text-center text-gray-500 text-xs">
            © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
        </p>

@endsection
