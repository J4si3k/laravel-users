@extends('admin.auth.wrapper')
@section('content')

<div
    class="bg-white text-gray-700 pl-4 font-bold uppercase bg-gray-lighter px-2 py-3 border-solid border-gray-light border-b">
    Zmień hasło
</div>

<form method="POST" action="{{ route('password.change.update') }}" class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
    @csrf
    @method('PUT')

    <div class="mb-4">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="current_password">
            Current Password
        </label>
        <input id="current_password" type="password"
            class="@error('current_password') border-red-500 @enderror shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            name="current_password" value="{{ $current_password ?? old('current_password') }}" required autocomplete="current_password" autofocus>

        @error('current_password')
            <p class="text-red-500 text-xs italic">{{ $message }}</p>
        @enderror
    </div>
    <div class="mb-4">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
            New Password
        </label>
        <input
            class="@error('password') border-red-500 @enderror shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            id="password" name="password" type="password">
        
        @error('password')
            <p class="text-red-500 text-xs italic">{{ $message }}</p>
        @enderror
    </div>
    <div class="mb-4">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="password_confirmation">
            Confirm New Password
        </label>
        <input
            class="@error('password_confirmation') border-red-500 @enderror shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            id="password_confirmation" name="password_confirmation" type="password">
        
        @error('password_confirmation')
            <p class="text-red-500 text-xs italic">{{ $message }}</p>
        @enderror
    </div>
    <div class="flex items-center justify-between">
        <button
            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            type="submit">
            change Password
        </button>
    </div>
</form>

@endsection
