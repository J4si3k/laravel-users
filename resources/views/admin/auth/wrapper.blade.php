@include('admin.parts.header')
<body class="bg-gray-200">
    <div id="adminapp">
        <div class="flex h-screen">
            <div class="m-auto w-full max-w-xs">
                @include('admin.parts.flash')
                <br>
                @yield('content')
            </div>
        </div>
    </div>
    <script src="{{ mix('js/admin.js') }}"></script>
</body>
</html>