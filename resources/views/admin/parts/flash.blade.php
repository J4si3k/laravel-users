@if (session()->has('success'))
    @component('admin.components.flash', ['color' => 'blue'])
        {!! session()->pull('success') !!}
    @endcomponent
@endif

@if (Session::has('error'))
    @component('admin.components.flash', ['color' => 'red'])
        {!! session()->pull('error') !!}
    @endcomponent
@endif