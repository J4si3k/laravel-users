@isset($route_edit)
    <a href="{{ route(...$route_edit) }}" class="text-sm text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline bg-blue-500 hover:bg-blue-700 mr-3">Edytuj</a>
@endisset


@isset($route_preview)
    <a href="{{ route(...$route_preview) }}" class="text-sm text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline bg-teal-500 hover:bg-teal-700 mr-3">Podgląd</a>
@endisset


@isset($route_send)
<form method="POST" action="{{ route(...$route_send) }}" class="inline-block">
    @csrf
    @method('POST')
    <button type="submit" class="text-sm text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline bg-green-500 hover:bg-green-700">
        wyślij
    </button>
</form>
@endisset


@isset($route_delete)
    <form onclick="return confirm('{{__('app.are_you_sure_delete', ['value' => $alert_delete_text])}}')" method="POST" action="{{ route(...$route_delete) }}" class="inline-block">
        @method('DELETE')
        @csrf
        <button type="submit" class="text-sm text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline bg-red-500 hover:bg-red-700">Usuń</button>
    </form>
@endisset