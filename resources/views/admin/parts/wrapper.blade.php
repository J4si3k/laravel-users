@include('admin.parts.header')
<body class="bg-gray-200">
    <div id="adminapp">
        @include('admin.parts.menu')

        <div class="container w-full mx-auto pt-20">
            <div class="w-full px-4 md:px-0 md:mt-8 mb-16 text-gray-800 leading-normal">
                @include('admin.parts.flash')
                @yield('content')

            </div>
        </div>
        
    </div>
    <script src="{{ mix('js/admin.js') }}"></script>
    @include('admin.parts.ckeditor')
</body>
</html>