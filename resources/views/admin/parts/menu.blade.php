<nav id="header" class="bg-white fixed w-full z-10 top-0 shadow">
	

    <div class="w-full container mx-auto flex flex-wrap items-center mt-0 pt-3 pb-3 lg:pb-0 md:pb-3">
            
        <div class="w-2/3 pl-2 md:pl-0">
            <a class="text-gray-900 text-base xl:text-xl no-underline hover:no-underline font-bold"  href="{{ url('/') }}"> 
                <div class="float-left mr-4">
                    {{-- <img src="{{ config('portal.portal_logo') }}" alt="">  --}}
                </div>
            </a>
            <a href="{{ route('admin.dashboard.accounts.list') }}">
                <div class="text-portal leading-loose font-extrabold inline-block align-middle text-xl sm:text-2xl">
                    {{ config('portal.portal_title') }}
                </div>
            </a>
        </div>
        <div class="w-1/3 pr-0">
            <div class="flex relative inline-block float-right">
            
              @guest
                  niezalogowany
              @else
              <div class="relative text-sm py-2">
                <button id="userButton" class="inline-block align-middle h-8 flex items-center focus:outline-none mr-5">
                  <span class="hidden md:inline-block">zalogowany: {{ Auth::user()->name }} </span>
                  <svg class="pl-2 h-2" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"/></g></svg>
                </button>
                <div id="userMenu" class="bg-white rounded shadow-md mt-2 absolute mt-12 top-0 right-0 min-w-full overflow-auto z-30 hidden">
                    <ul class="list-reset border border-solid border-gray-200">
                    <li><a href="{{ route('password.change') }}" class="px-4 py-2 block text-gray-900 hover:bg-gray-400 no-underline hover:no-underline">zmień hasło</a></li>
                      {{-- <li><hr class="border-t mx-2 border-gray-400"></li> --}}
                      <li><a onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" href="{{ route('logout') }}" class="px-4 py-2 block text-gray-900 hover:bg-gray-400 no-underline hover:no-underline"> {{ __('Logout') }}</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                </div>
            </div>
              @endguest


                <div class="block py-2 lg:hidden pr-4">
                <button id="nav-toggle" class="flex items-center px-3 py-2 border rounded text-gray-500 border-gray-600 hover:text-gray-900 hover:border-teal-500 appearance-none focus:outline-none">
                    <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
                </button>
            </div>
            </div>
 
        </div>


        <div class="w-full flex-grow lg:flex lg:items-center lg:w-auto hidden lg:block mt-2 lg:mt-0 bg-white z-20" id="nav-content">
            <ul class="list-reset lg:flex flex-1 items-center px-4 md:px-0">

                <li class="mr-6 md:m-2 md:my-0">
                    <a href="{{ route('admin.dashboard.accounts.list') }}" class="{{ routeContains('accounts') ? 'text-blue-600 border-blue-600' : ' text-gray-500 ' }} block py-1 md:py-3 pl-1 align-middle capitalize no-underline hover:text-gray-900 border-b-2 hover:border-blue-600">
                        <i class="fas fa-user fa-fw mr-3 {{ routeContains('accounts') ? 'text-blue-600' : '' }}"></i><span class="pb-1 md:pb-0 text-sm">Accounts</span>
                    </a>
                </li> 
            
            </ul>
            

            
        </div>
        
    </div>
</nav>