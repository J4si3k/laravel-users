@extends('admin.parts.wrapper')
@section('content')

<br>

<div id="list-wrapper">
    @if (count($accounts))
    <table class="w-full text-md bg-white shadow-md rounded mb-4">
        <thead>
            <tr class="border-b">
                <th data-sort="s-id" class="sort cursor-pointer uppercase text-left leading-none p-3 px-5">id <i class="fas fa-sort"></i></th>
                <th data-sort="s-name" class="sort cursor-pointer uppercase text-left leading-none p-3 px-5">name <i class="fas fa-sort"></i></th>
                <th data-sort="s-surname" class="sort cursor-pointer uppercase text-left leading-none p-3 px-5">surname <i class="fas fa-sort"></i></th>
                <th data-sort="s-surname" class="sort cursor-pointer uppercase text-left leading-none p-3 px-5 hidden md:table-cell">position <i class="fas fa-sort"></i></th>
                <th data-sort="s-update" class="sort cursor-pointer uppercase text-left leading-none p-3 px-5 hidden md:table-cell">Zaktualizowano <i class="fas fa-sort"></i></th>
                <th class="uppercase text-left leading-none p-3 px-5">akcje</th>
            </tr>
        </thead>
        <tbody class="list">
            @foreach ($accounts as $key => $account)
            
            <tr class="border-b hover:bg-blue-100 ">
                <td class="s-id p-3 px-5">{{ $account->id }}</td>
                <td class="s-name p-3 px-5">{{ $account->name }}</td>
                <td class="s-surname p-3 px-5">{{ $account->surname }}</td>
                <td class="s-question p-3 px-5 hidden md:table-cell">{{ $account->position->name ?? '' }}</td>
                <td class="s-update p-3 px-5 hidden md:table-cell">{{ humanize_date($account->updated_at, 'd.m.Y, H:i') }}</td>
                <td class="p-3 px-5 flex justify-left">
                    @include('admin.parts.actions', [
                        'route_edit' => ['admin.dashboard.accounts.edit', $account->id],
                        'route_delete' => ['admin.dashboard.accounts.destroy', $account->id],
                        'alert_delete_text' => $account->name
                    ])
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>

@endsection