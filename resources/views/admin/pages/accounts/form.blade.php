@extends('admin.parts.wrapper')
@section('content')
    
    <br>
    @component('admin.components.form.wrapper', [
        'title' => isset($account) ? 'Edytuj dane użytkownika' : 'Dodaj użytkownika', 
        'route' => isset($account) ? ['admin.dashboard.accounts.update', $account] : ['admin.dashboard.accounts.store']
    ])

        <div class="flex flex-wrap -mx-3 mb-2">
            @component('admin.components.form.group', ['parts' => '1/2', 'label' => 'Imię', 'message' => $errors->first('name') ])
                <input value="{{$account->name ?? old('name') }}" name="name" class="appearance-none block w-full bg-gray-100 text-gray-600er border border-red rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text">
            @endcomponent
            @component('admin.components.form.group', ['parts' => '1/2', 'label' => 'Nazwisko', 'message' => $errors->first('surname') ])
                <input value="{{$account->surname ?? old('surname') }}" name="surname" class="appearance-none block w-full bg-gray-100 text-gray-600er border border-red rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text">
            @endcomponent
        </div>
        <div class="flex flex-wrap -mx-3 mb-2">
            @component('admin.components.form.group', ['label' => 'Opis', 'message' => $errors->first('description')])
                <textarea name="description" class="editor" cols="30" rows="10" >
                    {{ $account->description ?? old('description') }}
                </textarea>
            @endcomponent
        </div>
        <div class="flex flex-wrap -mx-3 mb-2">
            @foreach ($account->skills as $key => $skill)
                @component('admin.components.form.group', ['parts' => '1/3', 'label' => $skill->name, 'message' => $errors->first('surname') ])
                @switch($skill->type)
                    @case('text')
                        <input type="hidden" name="skills[<?= $key ?>][type]" value="text">
                        <input value="{{ $skill->pivot->skill_value ?? '' }}" name="skills[<?= $key ?>][skill_value]" class="appearance-none block w-full bg-gray-100 text-gray-600er border border-red rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text">
                    @break
                    @case('checkbox')
                        <div class="flex items-center">
                            <input value="checkbox" name="skills[<?= $key ?>][type]" type="hidden">
                            <input value="0" name="skills[<?= $key ?>][skill_value]" type="hidden">
                            <input value="1" name="skills[<?= $key ?>][skill_value]" id="skill_value" {{ $skill->pivot->skill_value ? "checked=checked" : ''}} type="checkbox" class="form-checkbox border-2 border-gray-400">
                            <label for="skill_value" class="py-3 ml-2">{{ $skill->name }}</label>
                        </div>
                    @break
                    @endswitch
                @endcomponent
            @endforeach
        </div>

        <button type="submit" class="btn bg-blue-600 text-white p-2 rounded-md">
            Zapisz wszystko
        </button>
    
    @endcomponent
    <br>

@endsection