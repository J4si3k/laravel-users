<div class="md:w-1/3 md:w-1/2 hidden"></div>
<div class="w-full md:w-{{ isset($parts) ? $parts : '1/1'}} px-3 mb-6 md:mb-0">
    <label style="{{ empty($label) ? 'margin-top: 22px;' : '' }}" class="block uppercase tracking-wide text-gray-600er text-xs font-light mb-1">
        @isset($label)
            {{ $label }}
        @endisset
        
    </label>
    {{ $slot }}
    @isset($message)
        <p class="text-red-500 text-xs italic">{{ $message }}</p>
    @endisset
</div>