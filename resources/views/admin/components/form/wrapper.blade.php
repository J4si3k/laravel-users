<div class="bg-white border-solid border-gray-light rounded border shadow-sm w-full">
    @isset($title)
        <div class="pl-4 font-bold uppercase bg-gray-lighter px-2 py-3 border-solid border-gray-light border-b">
                {{ $title }}
        </div>
    @endisset
    <div class="{{ isset($small) ? 'px-3' : 'p-3' }}">
        <form method="POST" action="{{ route(...$route) }}" class="w-full" {{ (isset($hasFile) && $hasFile) ? "enctype=multipart/form-data" : ''}}>
            @csrf
            @if (Str::contains($route[0], 'update'))
                @method('PATCH')
            @endif

        {{ $slot }}
        </form>
    </div>
</div>