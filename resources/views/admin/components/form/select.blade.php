<div class="relative">
<select name="{{ $name ?? 'SET_FIELD_NAME!'}}" {{isset($disabled) ? 'disabled="disabled"' : ''}} class="{{isset($disabled) ? 'cursor-not-allowed text-gray-400' : ''}} block appearance-none w-full bg-gray-100 border border-gray-lighter text-gray-600er py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey">
        {{ $slot }}
    </select>
    <div class="py-4 absolute right-0 top-0 pointer-events-none absolute pin-y pin-r flex items-center px-2 text-grey">
        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"></path></svg>
    </div>
</div>