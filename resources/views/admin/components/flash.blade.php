<div class="flex relative pt-4">
    <div id="flash-message" role="alert" class="min-w-full bg-{{ isset($color) ? $color : 'blue' }}-500 border border-{{ isset($color) ? $color : 'blue' }}-light text-white pl-4 pr-8 py-3 rounded relative">
        <span class="block sm:inline">{{ $slot }}</span> 
        <span onclick="document.getElementById('flash-message').style.display = 'none'" class="absolute mr-4 right-0 inset-y-1/2 h-6 w-6 -mt-3">
            <svg role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                <title>Close</title><path stroke="#eee" fill="white" d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"></path>
            </svg>
        </span>
    </div>
</div>