import List from 'list.js';

window.Vue = require('vue');

new Vue({
    el: '#adminapp',
    mounted() {
      this.setupTables();
    },
    methods: {
      setupTables() {

        let ths = document.querySelectorAll('table thead th');
        let valueNames = ths && [...ths].map((th) => th.dataset.sort)
                                        .filter(el => el!= undefined);
        var options = { valueNames };
        window.userList = new List('list-wrapper', options);
      }
    }
});



/*Toggle dropdown list*/
export const toggleClass = (el, ...cls) => cls.map(cl => el.classList.toggle(cl)); 
  
var navMenuDiv = document.getElementById("nav-content");
var navMenu = document.getElementById("nav-toggle");
var userMenuDiv = document.getElementById("userMenu");
var userMenu = document.getElementById("userButton")

navMenu && navMenu.addEventListener('click', () => toggleClass(navMenuDiv,"hidden","block"))
userMenu && userMenu.addEventListener('click', () => toggleClass(userMenuDiv,"hidden","block"))