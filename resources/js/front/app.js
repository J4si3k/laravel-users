import RegisterForm from './components/RegisterForm.vue';
import Vuelidate from 'vuelidate'

window._ = require('lodash');
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.Vue = require('vue');

Vue.component('register-form', RegisterForm);
Vue.use(Vuelidate)

new Vue({
    el: '#frontapp',
    mounted() {},
});