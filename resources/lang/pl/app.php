<?php 

return [
    'are_you_sure_delete' => 'jesteś pewny, że chcesz usunąć :value?',
    'name' => 'Imię',
    'surname' => 'Nazwisko',
    'description' => 'Opis',
    'skill_value' => '',
    'yes' => 'tak',
    'no' => 'nie'
];