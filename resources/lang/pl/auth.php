<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Te dane uwierzytelniające nie pasują do naszych danych.',
    'throttle' => 'Zbyt wiele prób logowania. Spróbuj ponownie za :seconds sekund.',
    'login' => 'Zaloguj',
    'login_to_panel' => 'Zaloguj do panelu szkoleń',
    'email_address' => 'Adres email',
    'password' => 'Hasło',
    'confirm_password' => 'Powtórz hasło',
    'name' => 'Nazwa',
    'remember_me' => 'Zapamiętaj dane logowania',
    'forgot_your_password' => 'Zapomniałeś hasła?',
    'register' => 'Zarejestruj się',
    'send_reset_link' => 'Wyślij link resetowania hasła',
    'reset_password' => 'Resetuj Hasło',
    'verify_email_heading' => 'Zweryfikuj swój adres email',
    'verify_email_send_info' => 'Świeży link weryfikacyjny został wysłany na Twój adres e-mail.',
    'verify_email_check' => 'Przed kontynuowaniem sprawdź adres e-mail pod kątem linku weryfikacyjnego.',
    'verify_email_not_receive' => 'Jeśli nie otrzymałeś wiadomości e-mail',
    'verify_email_repeat' => 'kliknij tutaj, aby poprosić o kolejną',
];
