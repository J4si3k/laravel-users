<?php 

return [
    'are_you_sure_delete' => 'are you sure delete :value?',
    'name' => 'Name',
    'surname' => 'Surname',
    'description' => 'Description',
    'skill_value' => '',
    'yes' => 'yes',
    'no' => 'no',
];